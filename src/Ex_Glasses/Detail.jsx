import React, { Component } from 'react'

export default class Detail extends Component {
  render() {
    let {price, desc, name} = this.props.detailGlass
    return (
        <>      
            <div className="row">
                <div className="col-6"></div>     
                <div className='col-6 text-left bg-primary text-white p-0'>
                    <h3 className='pl-3 pt-2'>Name: {name}</h3>
                    <p className='mb-2 pl-3'>Price: {price}</p>
                    <p className='mb-1 pl-3'>Description: {desc}</p>
                </div>
            </div>
        </>
    )
  }
}
