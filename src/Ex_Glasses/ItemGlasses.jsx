import React, { Component } from 'react'

export default class ItemGlasses extends Component {
  render() {
      let {url} = this.props.data
    return (
      <img onClick={() => {
        this.props.handleClick(this.props.data)
      }} className='w-100 col-4 mb-3' src={url} alt="Không tìm thấy hình ảnh" />
    )
  }
}
