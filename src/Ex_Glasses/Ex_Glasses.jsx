import React, { Component } from 'react'
import { dataGlasses } from './dataGlasses'
import Detail from './Detail'
import ItemGlasses from './ItemGlasses'
import Model from './Model'

export default class Ex_Glasses extends Component {

  state = {
    glassesArr: dataGlasses,
    detail: dataGlasses[0],
  }

  handleChangeDetail = (glass) => {
    this.setState({
      detail: glass,
    })
  }
  
  renderGlassList = () => {
    return this.state.glassesArr.map((item, index) => {
      return <ItemGlasses
        handleClick = {this.handleChangeDetail}
        key = {index}
        data = {item}
      />
    })
  }

  render() {
    return (
      <div className='container'>
        <div className="row mt-3">
          <div className='col-6'>
            {this.renderGlassList()}  
          </div>           
          <Model/>
        </div>
        <Detail detailGlass = {this.state.detail}/>
      </div>
    )
  }
}
